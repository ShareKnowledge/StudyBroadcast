package com.example.it.studybroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by 鲁迅认识的那只猹 on 7/20/2017 3:20 PM.
 * Class Desc:
 * <p>
 * Update
 * Updated by IT on 7/20/2017 3:20 PM
 * Desc:
 */

public class MyBootComplatedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent gotoIntent = new Intent(context, RootComplatedActivity.class);
        context.startActivity(gotoIntent);
    }
}
