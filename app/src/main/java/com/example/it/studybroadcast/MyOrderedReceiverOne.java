package com.example.it.studybroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyOrderedReceiverOne extends BroadcastReceiver {
    public MyOrderedReceiverOne() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String value = intent.getStringExtra("MyKey");
        Toast.makeText(context, value, Toast.LENGTH_SHORT).show();

        //添加数据
        this.setResultData("我是添加的内容!");
    }
}
