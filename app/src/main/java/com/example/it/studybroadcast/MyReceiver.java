package com.example.it.studybroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String value = intent.getStringExtra("MyKey");
        Toast.makeText(context, value, Toast.LENGTH_SHORT).show();
    }
}
