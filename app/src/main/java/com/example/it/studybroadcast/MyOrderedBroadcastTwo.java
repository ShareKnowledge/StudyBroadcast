package com.example.it.studybroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyOrderedBroadcastTwo extends BroadcastReceiver {
    public MyOrderedBroadcastTwo() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, getResultData(), Toast.LENGTH_SHORT).show();
        //截断我们的广播
        this.abortBroadcast();
    }
}
